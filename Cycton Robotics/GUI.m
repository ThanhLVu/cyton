function varargout = GUI(varargin)
% GUI MATLAB code for GUI.fig
%      GUI, by itself, creates a new GUI or raises the existing
%      singleton*.
%
%      H = GUI returns the handle to a new GUI or the handle to
%      the existing singleton*.
%
%      GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GUI.M with the given input arguments.
%
%      GUI('Property','Value',...) creates a new GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before GUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to GUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help GUI

% Last Modified by GUIDE v2.5 15-Oct-2018 19:38:16

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @GUI_OpeningFcn, ...
                   'gui_OutputFcn',  @GUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before GUI is made visible.
function GUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to GUI (see VARARGIN)

% Choose default command line output for GUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes GUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = GUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in Start.
function Start_Callback(hObject, eventdata, handles)
clear all;
run('Test.m');
% hObject    handle to Start (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in Stop.
function Stop_Callback(hObject, eventdata, handles)
% If the button is pressed
handles.stopButton = 1;
% Update guidata
guidata(hObject, handles);
% Create an infinite loop to interfere the current action
while(handles.stopButton == 1)
    drawnow;
end
% hObject    handle to Stop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in Move.
function Move_Callback(hObject, eventdata, handles)
% hObject    handle to Move (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Evironment 
workspace = [-1 1 -1 1 -0.1 1];
% Robot's start position
firstPose = [0 0 0 0 0 0 0];
% Robot's base position
basePosition = [0 0 0];
% Import Cyton robot
robot1 = Cyton('Robot1',basePosition,workspace,firstPose);
% Get desired pose of joints
ratioOne = get(handles.jointSlideOne,'value');
limOne = deg2rad(150);
jointOne = limOne * ratioOne;
ratioTwo = get(handles.jointSlideTwo,'value');
limTwo = deg2rad(105);
jointTwo = limTwo * ratioTwo;
ratioThree = get(handles.jointSlideThree,'value');
limThree = deg2rad(150);
jointThree = limThree * ratioThree;
ratioFour = get(handles.jointSlideFour,'value');
limFour = deg2rad(105);
jointFour = limFour * ratioFour;
ratioFive = get(handles.jointSlideFive,'value');
limFive = deg2rad(105);
jointFive = limFive * ratioFive;
ratioSix = get(handles.jointSlideSix,'value');
limSix = deg2rad(105);
jointSix = limSix * ratioSix;
ratioSeven = get(handles.jointSlideSeven,'value');
limSeven = deg2rad(105);
jointSeven = limSeven * ratioSeven;

q = [jointOne jointTwo jointThree jointFour jointFive jointSix jointSeven];
% Forward kinematics to calculate the end effector position
T = robot1.model.fkine(q);
% Display the calculated end effector position
handles.eeX.String = num2str(T(1,4));
handles.eeY.String = num2str(T(2,4));
handles.eeZ.String = num2str(T(3,4));
% Display current joints' pose if using slider
handles.jointOne.String = num2str(rad2deg(jointOne));
handles.jointTwo.String = num2str(rad2deg(jointTwo));
handles.jointThree.String = num2str(rad2deg(jointThree));
handles.jointFour.String = num2str(rad2deg(jointFour));
handles.jointFive.String = num2str(rad2deg(jointFive));
handles.jointSix.String = num2str(rad2deg(jointSix));
handles.jointSeven.String = num2str(rad2deg(jointSeven));
% Move the robot
qMatrix = jtraj(firstPose,q,50);
robot1.model.animate(qMatrix);



function jointOne_Callback(hObject, eventdata, handles)
% hObject    handle to jointOne (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of jointOne as text
%        str2double(get(hObject,'String')) returns contents of jointOne as a double


% --- Executes during object creation, after setting all properties.
function jointOne_CreateFcn(hObject, eventdata, handles)
% hObject    handle to jointOne (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function jointTwo_Callback(hObject, eventdata, handles)
% hObject    handle to jointTwo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of jointTwo as text
%        str2double(get(hObject,'String')) returns contents of jointTwo as a double


% --- Executes during object creation, after setting all properties.
function jointTwo_CreateFcn(hObject, eventdata, handles)
% hObject    handle to jointTwo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function jointThree_Callback(hObject, eventdata, handles)
% hObject    handle to jointThree (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of jointThree as text
%        str2double(get(hObject,'String')) returns contents of jointThree as a double


% --- Executes during object creation, after setting all properties.
function jointThree_CreateFcn(hObject, eventdata, handles)
% hObject    handle to jointThree (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function jointFour_Callback(hObject, eventdata, handles)
% hObject    handle to jointFour (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of jointFour as text
%        str2double(get(hObject,'String')) returns contents of jointFour as a double


% --- Executes during object creation, after setting all properties.
function jointFour_CreateFcn(hObject, eventdata, handles)
% hObject    handle to jointFour (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function jointFive_Callback(hObject, eventdata, handles)
% hObject    handle to jointFive (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of jointFive as text
%        str2double(get(hObject,'String')) returns contents of jointFive as a double


% --- Executes during object creation, after setting all properties.
function jointFive_CreateFcn(hObject, eventdata, handles)
% hObject    handle to jointFive (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function jointSix_Callback(hObject, eventdata, handles)
% hObject    handle to jointSix (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of jointSix as text
%        str2double(get(hObject,'String')) returns contents of jointSix as a double


% --- Executes during object creation, after setting all properties.
function jointSix_CreateFcn(hObject, eventdata, handles)
% hObject    handle to jointSix (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function jointSeven_Callback(hObject, eventdata, handles)
% hObject    handle to jointSeven (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of jointSeven as text
%        str2double(get(hObject,'String')) returns contents of jointSeven as a double


% --- Executes during object creation, after setting all properties.
function jointSeven_CreateFcn(hObject, eventdata, handles)
% hObject    handle to jointSeven (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function eeX_Callback(hObject, eventdata, handles)
% hObject    handle to eeX (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of eeX as text
%        str2double(get(hObject,'String')) returns contents of eeX as a double


% --- Executes during object creation, after setting all properties.
function eeX_CreateFcn(hObject, eventdata, handles)
% hObject    handle to eeX (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function eeY_Callback(hObject, eventdata, handles)
% hObject    handle to eeY (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of eeY as text
%        str2double(get(hObject,'String')) returns contents of eeY as a double


% --- Executes during object creation, after setting all properties.
function eeY_CreateFcn(hObject, eventdata, handles)
% hObject    handle to eeY (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function eeZ_Callback(hObject, eventdata, handles)
% hObject    handle to eeZ (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of eeZ as text
%        str2double(get(hObject,'String')) returns contents of eeZ as a double


% --- Executes during object creation, after setting all properties.
function eeZ_CreateFcn(hObject, eventdata, handles)
% hObject    handle to eeZ (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in ikMovement.
function ikMovement_Callback(hObject, eventdata, handles)
% hObject    handle to ikMovement (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Evironment 
workspace = [-1 1 -1 1 -0.1 1];
% Robot's start position
firstPose = [0 0 0 0 0 0 0];
% Robot's base position
basePosition = [0 0 0];
% Import Cyton robot
robot1 = Cyton('Robot1',basePosition,workspace,firstPose);
% Get input data for end effector
eeX = str2double(handles.eeX.String);
eeY = str2double(handles.eeX.String);
eeZ = str2double(handles.eeX.String);

T = [1 0 0 eeX;
     0 1 0 eeY;
     0 0 1 eeZ;
     0 0 0 1];
% Inverse kinematic to get joint poses
J = robot1.model.ikcon(T)*180/pi;
% Display calculated joint poses
handles.jointOne.String = num2str(round(J(1)));
handles.jointTwo.String = num2str(round(J(2)));
handles.jointThree.String = num2str(round(J(3)));
handles.jointFour.String = num2str(round(J(4)));
handles.jointFive.String = num2str(round(J(5)));
handles.jointSix.String = num2str(round(J(6)));
handles.jointSeven.String = num2str(round(J(7)));
% Move the robot
qMatrix = jtraj(firstPose,J,50);
robot1.model.animate(qMatrix);


% --- Executes on slider movement.
function jointSlideTwo_Callback(hObject, eventdata, handles)
% hObject    handle to jointSlideTwo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

% --- Executes during object creation, after setting all properties.
function jointSlideTwo_CreateFcn(hObject, eventdata, handles)
% hObject    handle to jointSlideTwo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function jointSlideOne_Callback(hObject, eventdata, handles)
% hObject    handle to jointSlideOne (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function jointSlideOne_CreateFcn(hObject, eventdata, handles)
% hObject    handle to jointSlideOne (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function jointSlideThree_Callback(hObject, eventdata, handles)
% hObject    handle to jointSlideThree (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function jointSlideThree_CreateFcn(hObject, eventdata, handles)
% hObject    handle to jointSlideThree (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function jointSlideFour_Callback(hObject, eventdata, handles)
% hObject    handle to jointSlideFour (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function jointSlideFour_CreateFcn(hObject, eventdata, handles)
% hObject    handle to jointSlideFour (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function jointSlideFive_Callback(hObject, eventdata, handles)
% hObject    handle to jointSlideFive (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function jointSlideFive_CreateFcn(hObject, eventdata, handles)
% hObject    handle to jointSlideFive (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function jointSlideSix_Callback(hObject, eventdata, handles)
% hObject    handle to jointSlideSix (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function jointSlideSix_CreateFcn(hObject, eventdata, handles)
% hObject    handle to jointSlideSix (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function jointSlideSeven_Callback(hObject, eventdata, handles)
% hObject    handle to jointSlideSeven (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function jointSlideSeven_CreateFcn(hObject, eventdata, handles)
% hObject    handle to jointSlideSeven (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
