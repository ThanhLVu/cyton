classdef ShotOneLink < handle
    properties
        %> Robot model
        model;
        
        %> workspace
        workspace = [-1.4 1.4 -1.4 1.4 -1 1];   
        
        %> base position
        basePosition = [0 0 0];
        
        %> name
        name = [];
        
        %> If we have a tool model which will replace the final links model, combined ply file of the tool model and the final link models
        toolModelFilename = []; % Available are: 'DabPrintNozzleTool.ply';        
        toolParametersFilename = []; % Available are: 'DabPrintNozzleToolParameters.mat';        
    end
    
    methods%% Class for one link simulation
        function self = ShotOneLink(robotName,basePosition,workspace)
            % Set inputs for the robot
            self.name = robotName;
            self.basePosition = basePosition;
            self.workspace = workspace
            
            self.GetOneLink();
            self.PlotAndColourRobot();%robot,workspace);
        end

        %% GetOneLink
        % Given a name (optional), create and return a one link model
        function GetOneLink(self)
            robotName = self.name;

            % DH parameters for an object
            L1 = Link('d',0,'a',0,'alpha',0,'qlim',[-pi pi]);

            % Create object
            self.model = SerialLink(L1,'name',robotName);
            self.model.base = transl(self.basePosition);
            self.model.delay = 0;
        end

        %% PlotAndColourRobot
        % Given a robot index, add the glyphs (vertices and faces) and
        % colour them in if data is available 
        function PlotAndColourRobot(self)%robot,workspace)
            for linkIndex = 0:self.model.n
                [ faceData, vertexData, plyData{linkIndex + 1} ] = plyread([self.name,'.ply'],'tri'); %#ok<AGROW>
                self.model.faces{linkIndex + 1} = faceData;
                self.model.points{linkIndex + 1} = vertexData;
            end

            % Display object            
            self.model.plot3d(0,'noarrow','workspace',self.workspace,'scale',0.5,'delay',0);
            if isempty(findobj(get(gca,'Children'),'Type','Light'))
            end  
            self.model.delay = 0;

            % Try to correctly colour the arm (if colours are in ply file data)
            for linkIndex = 0:self.model.n
                handles = findobj('Tag', self.model.name);
                h = get(handles,'UserData');
                try 
                    h.link(linkIndex+1).Children.FaceVertexCData = [plyData{linkIndex+1}.vertex.red ...
                                                                  , plyData{linkIndex+1}.vertex.green ...
                                                                  , plyData{linkIndex+1}.vertex.blue]/255;
                    h.link(linkIndex+1).Children.FaceColor = 'interp';
                catch ME_1
                    disp(ME_1);
                    continue;
                end
            end
        end 
    end
end