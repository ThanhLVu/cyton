function Safety()
%Loading the environment
hold on;
%Table Model
PlaceObject('table.ply', [0 0 -1]);
%Fence Models
PlaceObject('fence.ply', [0.42 0.75 0]);
PlaceObject('fence.ply', [-0.42 0.75 0]);
PlaceObject('sideFence.ply', [0.85 0.35 0]);
PlaceObject('sideFence.ply', [0.85 -0.45 0]);
PlaceObject('sideFence.ply', [-0.85 0.35 0]);
PlaceObject('sideFence.ply', [-0.85 -0.45 0]);
%Warning Labels
PlaceObject('warning1.ply',[-0.5 -0.6 0.003]);
PlaceObject('warning2.ply',[0.5 -0.6 0.013]);
%Robot Wall
PlaceObject('wall.ply',[0.1 0 0]);
PlaceObject('wall.ply',[-0.1 0 0]);
PlaceObject('wall2.ply',[0 0.09 0]);
PlaceObject('wall2.ply',[0 -0.09 0]);
%Safety Cones
conePos = [1 1 -1; 1 -1 -1; -1 1 -1; -1 -1 -1]; %Cone positions
for i = 1:4
    PlaceObject('cone.ply', conePos(i,:));
end
%Cups
PlaceObject('cup.ply',[-0.187 0.108 0.005]);
view(3);
end