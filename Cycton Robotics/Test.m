function Test()
% Safety 
Safety();
% Evironment
workspace = [-1.4 1.4 -1.4 1.4 -1 1];

% Robot's start position
firstPose = [0 0 0 0 0 0 0];
% Robot's base position
basePosition = [0 0 0];
% Import Cyton robot
robot1 = Cyton('Robot1',basePosition,workspace,firstPose);
% robot1.model.teach;
% Shot glasses positions
shotOne = [-0.311 0 0.033];
shotTwo = [-0.22 -0.22 0.033];
shotThree = [0 -0.311 0.033];

% Pick up pose for 3 glasses
qPickOne = [0 deg2rad(70) 0 deg2rad(90) 0 deg2rad(-70) 0];
qPickTwo = [deg2rad(45) deg2rad(70) 0 deg2rad(90) 0 deg2rad(-70) 0];
qPickThree = [deg2rad(90) deg2rad(70) 0 deg2rad(90) 0 deg2rad(-70) 0];

% Mixer position
mixer = [-0.187 0.108 0.301];

% Mixer position pose
qMixer = [deg2rad(-30) deg2rad(-20) 0 deg2rad(90) 0 deg2rad(20) 0];

% Rise after picked up poses
qRiseOne = [deg2rad(0) deg2rad(-20) 0 deg2rad(90) 0 deg2rad(20) 0];
qRiseTwo = [deg2rad(45) deg2rad(-20) 0 deg2rad(90) 0 deg2rad(20) 0];
qRiseThree = [deg2rad(90) deg2rad(-20) 0 deg2rad(90) 0 deg2rad(20) 0];

% Pour the water out pose
qPour = [deg2rad(-30) deg2rad(-20) 0 deg2rad(90) 0 deg2rad(20) deg2rad(90)];

% Number of steps of each movement
steps = 50;

% Get real data, assign shot color
% [orderShotOne,orderShotTwo,orderShotThree] = CameraData()
orderShotOne = 3;
orderShotTwo = 2;
orderShotThree = 1;

% Create shots 3d model
switch orderShotOne
    case 1
        cupred = ShotOneLink('shotcupred',shotOne,[-1.4 1.4 -1.4 1.4 -1 1]);
    case 2
        cupgreen = ShotOneLink('shotcupgreen',shotOne,[-1.4 1.4 -1.4 1.4 -1 1]);
    case 3
        cupblue = ShotOneLink('shotcupblue',shotOne,[-1.4 1.4 -1.4 1.4 -1 1]);
end
switch orderShotTwo
    case 1
        cupred = ShotOneLink('shotcupred',shotTwo,[-1.4 1.4 -1.4 1.4 -1 1]);
    case 2
        cupgreen = ShotOneLink('shotcupgreen',shotTwo,[-1.4 1.4 -1.4 1.4 -1 1]);
    case 3
        cupblue = ShotOneLink('shotcupblue',shotTwo,[-1.4 1.4 -1.4 1.4 -1 1]);
end
switch orderShotThree
    case 1
        cupred = ShotOneLink('shotcupred',shotThree,[-1.4 1.4 -1.4 1.4 -1 1]);
    case 2
        cupgreen = ShotOneLink('shotcupgreen',shotThree,[-1.4 1.4 -1.4 1.4 -1 1]);
    case 3
        cupblue = ShotOneLink('shotcupblue',shotThree,[-1.4 1.4 -1.4 1.4 -1 1]);
end
% Red =1, Green = 2, Blue = 3
% Counter for number of glasses used
order = 1;
% Alert if an obstacle is detected
alert = false;

% Start pick and pour glasses from Red to Green to Blue
while(order<=3)
    if (order == 1)
        if (orderShotOne == 1)
            alert = GlassOne(robot1,firstPose,qPickOne,qRiseOne,qMixer,qPour,steps,alert,1,cupred,cupgreen,cupblue);
        end
        if (orderShotTwo == 1)
            alert = GlassTwo(robot1,firstPose,qPickTwo,qRiseTwo,qMixer,qPour,steps,alert,1,cupred,cupgreen,cupblue);
        end
        if (orderShotThree == 1)
            alert = GlassThree(robot1,firstPose,qPickThree,qRiseThree,qMixer,qPour,steps,alert,1,cupred,cupgreen,cupblue);
        end 
    end
    if (order == 2)
        if (orderShotOne == 2)
            alert = GlassOne(robot1,firstPose,qPickOne,qRiseOne,qMixer,qPour,steps,alert,2,cupred,cupgreen,cupblue);
        end
        if (orderShotTwo == 2)
            alert = GlassTwo(robot1,firstPose,qPickTwo,qRiseTwo,qMixer,qPour,steps,alert,2,cupred,cupgreen,cupblue);
        end
        if (orderShotThree == 2)
            alert = GlassThree(robot1,firstPose,qPickThree,qRiseThree,qMixer,qPour,steps,alert,2,cupred,cupgreen,cupblue);
        end 
    end
    if (order == 3)
        if (orderShotOne == 3)
            alert = GlassOne(robot1,firstPose,qPickOne,qRiseOne,qMixer,qPour,steps,alert,3,cupred,cupgreen,cupblue);
        end
        if (orderShotTwo == 3)
            alert = GlassTwo(robot1,firstPose,qPickTwo,qRiseTwo,qMixer,qPour,steps,alert,3,cupred,cupgreen,cupblue);
        end
        if (orderShotThree == 3)
            alert = GlassThree(robot1,firstPose,qPickThree,qRiseThree,qMixer,qPour,steps,alert,3,cupred,cupgreen,cupblue);
        end 
    end
    order = order + 1;
end
end

% Actions for glass one
function [alert] = GlassOne(robot1,firstPose,qPickOne,qRiseOne,qMixer,qPour,steps,alert,color,cupred,cupgreen,cupblue)
    alert = CheckCollisionMove(robot1,firstPose,qPickOne,steps,alert,false,color,cupred,cupgreen,cupblue);
    alert = CheckCollisionMove(robot1,qPickOne,qRiseOne,steps,alert,true,color,cupred,cupgreen,cupblue);
    alert = CheckCollisionMove(robot1,qRiseOne,qMixer,steps,alert,true,color,cupred,cupgreen,cupblue);
    alert = CheckCollisionMove(robot1,qMixer,qPour,steps,alert,true,color,cupred,cupgreen,cupblue);
    alert = CheckCollisionMove(robot1,qPour,qMixer,steps,alert,true,color,cupred,cupgreen,cupblue);
    alert = CheckCollisionMove(robot1,qMixer,qRiseOne,steps,alert,true,color,cupred,cupgreen,cupblue);
    alert = CheckCollisionMove(robot1,qRiseOne,qPickOne,steps,alert,true,color,cupred,cupgreen,cupblue);
    alert = CheckCollisionMove(robot1,qPickOne,firstPose,steps,alert,false,color,cupred,cupgreen,cupblue);
end

% Actions for glass two
function [alert] = GlassTwo(robot1,firstPose,qPickTwo,qRiseTwo,qMixer,qPour,steps,alert,color,cupred,cupgreen,cupblue)
    alert = CheckCollisionMove(robot1,firstPose,qPickTwo,steps,alert,false,color,cupred,cupgreen,cupblue);
    alert = CheckCollisionMove(robot1,qPickTwo,qRiseTwo,steps,alert,true,color,cupred,cupgreen,cupblue);
    alert = CheckCollisionMove(robot1,qRiseTwo,qMixer,steps,alert,true,color,cupred,cupgreen,cupblue);
    alert = CheckCollisionMove(robot1,qMixer,qPour,steps,alert,true,color,cupred,cupgreen,cupblue);
    alert = CheckCollisionMove(robot1,qPour,qMixer,steps,alert,true,color,cupred,cupgreen,cupblue);
    alert = CheckCollisionMove(robot1,qMixer,qRiseTwo,steps,alert,true,color,cupred,cupgreen,cupblue);
    alert = CheckCollisionMove(robot1,qRiseTwo,qPickTwo,steps,alert,true,color,cupred,cupgreen,cupblue);
    alert = CheckCollisionMove(robot1,qPickTwo,firstPose,steps,alert,false,color,cupred,cupgreen,cupblue);
end

% Actions for glass three
function [alert] = GlassThree(robot1,firstPose,qPickThree,qRiseThree,qMixer,qPour,steps,alert,color,cupred,cupgreen,cupblue)
    alert = CheckCollisionMove(robot1,firstPose,qPickThree,steps,alert,false,color,cupred,cupgreen,cupblue);
    alert = CheckCollisionMove(robot1,qPickThree,qRiseThree,steps,alert,true,color,cupred,cupgreen,cupblue);
    alert = CheckCollisionMove(robot1,qRiseThree,qMixer,steps,alert,true,color,cupred,cupgreen,cupblue);
    alert = CheckCollisionMove(robot1,qMixer,qPour,steps,alert,true,color,cupred,cupgreen,cupblue);
    alert = CheckCollisionMove(robot1,qPour,qMixer,steps,alert,true,color,cupred,cupgreen,cupblue);
    alert = CheckCollisionMove(robot1,qMixer,qRiseThree,steps,alert,true,color,cupred,cupgreen,cupblue);
    alert = CheckCollisionMove(robot1,qRiseThree,qPickThree,steps,alert,true,color,cupred,cupgreen,cupblue);
    alert = CheckCollisionMove(robot1,qPickThree,firstPose,steps,alert,false,color,cupred,cupgreen,cupblue);
end

%% GetLinkPoses
% q - robot joint angles
% robot -  seriallink robot model
% transforms - list of transforms
function [ transforms ] = GetLinkPoses( q, robot)
links = robot.links;
transforms = zeros(4, 4, length(links) + 1);
transforms(:,:,1) = robot.base;

for i = 1:length(links)
    L = links(1,i);
    
    current_transform = transforms(:,:, i);
    
    current_transform = current_transform * trotz(q(1,i) + L.offset) * ...
    transl(0,0, L.d) * transl(L.a,0,0) * trotx(L.alpha);
    transforms(:,:,i + 1) = current_transform;
end
end

%% IsCollision
% This is based upon the output of questions 2.5 and 2.6
% Given a robot model (robot), and trajectory (i.e. joint state vector) (qMatrix)
% and triangle obstacles in the environment (faces,vertex,faceNormals)
function result = IsCollision(robot,qMatrix,faces,vertex,faceNormals,returnOnceFound)
if nargin < 6
    returnOnceFound = true;
end
result = false;

for qIndex = 1:size(qMatrix,1)
    % Get the transform of every joint (i.e. start and end of every link)
    tr = GetLinkPoses(qMatrix(qIndex,:), robot);

    % Go through each link and also each triangle face
    for i = 1 : size(tr,3)-1    
        for faceIndex = 1:size(faces,1)
            vertOnPlane = vertex(faces(faceIndex,1)',:);
            [intersectP,check] = LinePlaneIntersection(faceNormals(faceIndex,:),vertOnPlane,tr(1:3,4,i)',tr(1:3,4,i+1)');
            if check == 1 && IsIntersectionPointInsideTriangle(intersectP,vertex(faces(faceIndex,:)',:))
                result = true;
                if returnOnceFound
                    return
                end
            end
        end    
    end
end
end

%% IsIntersectionPointInsideTriangle
% Given a point which is known to be on the same plane as the triangle
% determine if the point is 
% inside (result == 1) or 
% outside a triangle (result ==0 )
function result = IsIntersectionPointInsideTriangle(intersectP,triangleVerts)

u = triangleVerts(2,:) - triangleVerts(1,:);
v = triangleVerts(3,:) - triangleVerts(1,:);

uu = dot(u,u);
uv = dot(u,v);
vv = dot(v,v);

w = intersectP - triangleVerts(1,:);
wu = dot(w,u);
wv = dot(w,v);

D = uv * uv - uu * vv;

% Get and test parametric coords (s and t)
s = (uv * wv - vv * wu) / D;
if (s < 0.0 || s > 1.0)        % intersectP is outside Triangle
    result = 0;
    return;
end

t = (uv * wu - uu * wv) / D;
if (t < 0.0 || (s + t) > 1.0)  % intersectP is outside Triangle
    result = 0;
    return;
end

result = 1;                      % intersectP is in Triangle
end

%% Check collision before movement
function [alert] = CheckCollisionMove(robot,q1,q2,steps,alert,carry,cup,cupred,cupgreen,cupblue)
    % If an obstacle has already been detected, return to main function
    if alert
        return;
    else
        
    % Create obstacle
    centerpnt = [-0.4,0,0];
    side = 0.2;
    plotOptions.plotFaces = true;
    [vertex,faces,faceNormals] = RectangularPrism(centerpnt-side/2, centerpnt+side/2,plotOptions);
    
    % Create movement matrix
    qMatrix = jtraj(q1,q2,steps);

    % Check if there is obstacle on movement path
    % If obstacle found, return without execute movement
    for i = 1: steps
        alert = IsCollision(robot.model,qMatrix(i,:),faces,vertex,faceNormals,false);
        if alert == 1
            break;
        end
    end
    
    % Move if possible
    if alert == 0 
        % If the robot is not carrying the shot glass
        if carry == 0
            robot.model.animate(qMatrix);
        end
        % If the robot is carrying the shot glass
        if carry == 1
            for i = 1:steps
                robot.model.animate(qMatrix(i,:));
                eePos = robot.model.fkine(qMatrix(i,:));
                % Carrying shot glass with given color
                switch cup
                    case 1
                        cupred.model.base = transl(eePos(1:3,4));
                        cupred.model.animate(0)
                    case 2
                        cupgreen.model.base = transl(eePos(1:3,4)');
                        cupgreen.model.animate(0)
                    case 3
                        cupblue.model.base = transl(eePos(1:3,4)');
                        cupblue.model.animate(0);
                end
            end
        end
    end
    end
end