classdef Cyton < handle
    properties
        %> Robot model
        model;
        
        %> workspace
        workspace;   
        
        %> base position
        basePosition;
        
        %> joint position
        q;
        
        %> name
        name = [];
               
        %> joint limit
        qlim = [];
        
        %> If we have a tool model which will replace the final links model, combined ply file of the tool model and the final link models
        toolModelFilename = []; % Available are: 'DabPrintNozzleTool.ply';        
        toolParametersFilename = []; % Available are: 'DabPrintNozzleToolParameters.mat';        
    end
    
    methods%% Class for Cyton robot simulation
        function self = Cyton(robotName,basePosition,workspace,q)
            % Set inputs for the robot
            self.name = robotName;
            self.basePosition = basePosition;
            self.workspace = workspace;
            self.q = q;
            
            self.GetCytonRobot();
            self.PlotAndColourRobot();%robot,workspace);
        end

        %% GetCytonRobot
        % Given a name (optional), create and return a Cyton robot model
        function GetCytonRobot(self)
            robotName = self.name;
            
            % DH parameters of the Cyton
            L1 = Link('d',0.12,'a',0,'alpha',pi/2,'qlim',[deg2rad(-150) deg2rad(150)]);
            L2 = Link('d',0,'a',0,'alpha',-pi/2,'qlim',[deg2rad(-105) deg2rad(105)]);
            L3 = Link('d',0.1408,'a',0,'alpha',pi/2,'qlim',[deg2rad(-150) deg2rad(150)]);
            L4 = Link('d',0,'a',0.0718,'alpha',-pi/2,'qlim',[deg2rad(-105) deg2rad(105)],'offset',pi/2);
            L5 = Link('d',0,'a',0.0718,'alpha',pi/2,'qlim',[deg2rad(-105) deg2rad(105)]);
            L6 = Link('d',0,'a',0,'alpha',-pi/2,'qlim',[deg2rad(-105) deg2rad(105)],'offset',-pi/2);
            L7 = Link('d',0.1296,'a',0,'alpha',0,'qlim',[deg2rad(-150) deg2rad(150)]);

            % Create arm with given inputs
            self.model = SerialLink([L1 L2 L3 L4 L5 L6, L7],'name',robotName);
            self.model.base = transl(self.basePosition);
            self.model.delay = 0;
            self.qlim = self.model.qlim;
        end

        %% PlotAndColourRobot
        % Given a robot index, add the glyphs (vertices and faces) and
        % colour them in if data is available 
        function PlotAndColourRobot(self)%robot,workspace)
            for linkIndex = 0:self.model.n
                [ faceData, vertexData, plyData{linkIndex + 1} ] = plyread(['part',num2str(linkIndex),'.ply'],'tri'); %#ok<AGROW>
                self.model.faces{linkIndex + 1} = faceData;
                self.model.points{linkIndex + 1} = vertexData;
            end
            
            % Display realistic robot             
            self.model.plot3d(self.q,'noarrow','workspace',self.workspace,'scale',0.5,'delay',0.001);
            if isempty(findobj(get(gca,'Children'),'Type','Light'))
                camlight;
            end  


            % Try to correctly colour the arm (if colours are in ply file data)
            for linkIndex = 0:self.model.n
                handles = findobj('Tag', self.model.name);
                h = get(handles,'UserData');
                try 
                    h.link(linkIndex+1).Children.FaceVertexCData = [plyData{linkIndex+1}.vertex.red ...
                                                                  , plyData{linkIndex+1}.vertex.green ...
                                                                  , plyData{linkIndex+1}.vertex.blue]/255;
                    h.link(linkIndex+1).Children.FaceColor = 'interp';
                catch ME_1
                    disp(ME_1);
                    continue;
                end
            end
        end
    end
end