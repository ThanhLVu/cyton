function [shotOne,shotTwo,shotThree]=CameraData()
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

obj = webcam(1);
    
data = snapshot(obj);

% Red color detection
red = imsubtract(data(:,:,1), rgb2gray(data)); 
red = medfilt2(red, [3 3]);             
red = im2bw(red,0.18);                   

% Remove all those pixels less than 300px
red = bwareaopen(red,300);

% Label all the connected components in the image.
bw = logical(red);

% Here we do the image blob analysis.
% We get a set of properties for each labeled region.
statsRed = regionprops(bw, 'BoundingBox', 'Centroid');

% Blue color detection
blue = imsubtract(data(:,:,3), rgb2gray(data)); 
blue = medfilt2(blue, [3 3]);             
blue = im2bw(blue,0.18);                   

% Remove all those pixels less than 300px
blue = bwareaopen(blue,300);

% Label all the connected components in the image.
bw = logical(blue);

% Here we do the image blob analysis.
% We get a set of properties for each labeled region.
statsBlue = regionprops(bw, 'BoundingBox', 'Centroid');

% Green color detection
green = imsubtract(data(:,:,2), rgb2gray(data)); 
green = medfilt2(green, [3 3]);             
green = im2bw(green,0.18);                   

% Remove all those pixels less than 300px
green = bwareaopen(green,300);

% Label all the connected components in the image.
bw = logical(green);

% Here we do the image blob analysis.
% We get a set of properties for each labeled region.
statsGreen = regionprops(bw, 'BoundingBox', 'Centroid');

% This is a loop to bound the red objects in a rectangular box.
for object = 1:length(statsRed)
    bcRed = statsRed(object).Centroid;
    xRed = bcRed(1)
end  

% Loop to display blue object
for object = 1:length(statsBlue)
    bcBlue = statsBlue(object).Centroid;
    xBlue = bcBlue(1)
end 

% Loop to display green object
for object = 1:length(statsGreen)
    bcGreen = statsGreen(object).Centroid;
    xGreen = bcGreen(1)
end  

% Get position correspond to the colors
[shotOne,shotTwo,shotThree] = Position(xRed,xGreen,xBlue)

end

function [shotOne,shotTwo,shotThree]=Position(xRed,xGreen,xBlue)
    % Sort the order of Red Green and Blue from left to right
    left = min([xRed xGreen xBlue]);
    right = max([xRed xGreen xBlue]);
    if (left == xRed)
        if (right == xBlue) 
            middle = xGreen;
        end
    end
    if (left == xGreen)
        if (right == xBlue) 
            middle = xRed;
        end
    end
    if (left == xRed)
        if (right == xGreen) 
            middle = xBlue;
        end
    end
    if (left == xBlue)
        if (right == xRed) 
            middle = xGreen;
        end
    end
    if (left == xBlue)
        if (right == xGreen) 
            middle = xRed;
        end
    end
    if (left == xGreen)
        if (right == xRed) 
            middle = xBlue;
        end
    end
    % Assign color to according glasses
    switch xRed
        case left
            shotOne = 1;
        case middle
            shotTwo = 1;
        case right
            shotThree = 1;
    end
    switch xGreen
        case left
            shotOne = 2;
        case middle
            shotTwo = 2;
        case right
            shotThree = 2;           
    end
    switch xBlue
        case left 
            shotOne = 3;
        case middle
            shotTwo = 3;
        case right
            shotThree = 3;
    end
end
