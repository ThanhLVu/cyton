function EnableMovement()
%Setup variables
cute_enable_robot_client = rossvcclient('enableCyton');
cute_enable_robot_msg = rosmessage(cute_enable_robot_client);
%To Enable/Disable the robot. 
%Note: Hold onto the arm when disabling the robot
cute_enable_robot_msg.TorqueEnable = true; %false
cute_enable_robot_client.call(cute_enable_robot_msg);
end
