%Move all joint to specific angle
function MoveAllJoint(q)
%Creating variables
cute_move_client = rossvcclient('/GoToPosition');
cute_move_msg = rosmessage(cute_move_client);
%Creating the position message
cute_move_msg.JointStates = q;
%Command the robot to move
cute_move_client.call(cute_move_msg);
end