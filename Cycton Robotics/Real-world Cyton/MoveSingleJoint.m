function MoveSingleJoint()
%Setup variables
cute_single_joint_client = rossvcclient('/MoveSingleJoint');
cute_single_joint_msg = rosmessage(cute_single_joint_client);
%Setting values and sending to the robot
cute_single_joint_msg.JointNumber = 1;% Joints 0-6
cute_single_joint_msg.Position = 0;% (Rads)
cute_single_joint_client.call(cute_single_joint_msg);
end

