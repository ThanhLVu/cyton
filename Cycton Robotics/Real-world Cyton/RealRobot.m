function RealRobot()
% Setup variables
cute_enable_robot_client = rossvcclient('enableCyton');
cute_enable_robot_msg = rosmessage(cute_enable_robot_client);
% To Enable/Disable the robot. 
% Note: Hold onto the arm when disabling the robot
cute_enable_robot_msg.TorqueEnable = true; %false
cute_enable_robot_client.call(cute_enable_robot_msg);

% Enable gripper
% Setup variables
cute_enable_gripper_client = rossvcclient('claw_controller/torque_enable');
cute_enable_gripper_msg = rosmessage(cute_enable_gripper_client);
% To Enable/Disable the gripper
cute_enable_gripper_msg.TorqueEnable = 1; %0
cute_enable_gripper_client.call(cute_enable_gripper_msg);

% Robot's start position
firstPose = [0 0 0 0 0 0 0];

% Positions of shot glasses
shotOne = [-0.311 0 0.033];
shotTwo = [-0.22 -0.22 0.033];
shotThree = [0 -0.311 0.033];

% Pick up pose for 3 glasses
qPickOne = [0 deg2rad(70) 0 deg2rad(90) 0 deg2rad(-70) deg2rad(90)];
qPickTwo = [deg2rad(45) deg2rad(70) 0 deg2rad(90) 0 deg2rad(-70) deg2rad(90)];
qPickThree = [deg2rad(90) deg2rad(70) 0 deg2rad(90) 0 deg2rad(-70) deg2rad(90)];

% Mixer position
mixer = [-0.187 0.108 0.301];

% Mixer position pose
qMixer = [deg2rad(-30) deg2rad(-20) 0 deg2rad(90) 0 deg2rad(20) deg2rad(90)];

% Rise after picked up poses
qRiseOne = [deg2rad(0) deg2rad(-20) 0 deg2rad(90) 0 deg2rad(20) deg2rad(90)];
qRiseTwo = [deg2rad(45) deg2rad(-20) 0 deg2rad(90) 0 deg2rad(20) deg2rad(90)];
qRiseThree = [deg2rad(90) deg2rad(-20) 0 deg2rad(90) 0 deg2rad(20) deg2rad(90)];

% Pour the water out pose
qPour = [deg2rad(-30) deg2rad(-20) 0 deg2rad(90) 0 deg2rad(20) 0];

[orderShotOne,orderShotTwo,orderShotThree] = CameraData()
% orderShotOne = 3;
% orderShotTwo = 2;
% orderShotThree = 1;

% Move to first pose
MoveAllJoint(firstPose);
Claw(26);

% Counter for number of glasses used
order = 1;

% Start pick and pour glasses from Red to Green to Blue
while(order<=3)
    if (order == 1)
        if (orderShotOne == 1)
            MoveReal(firstPose,qPickOne,qRiseOne,qMixer,qPour);
        end
        if (orderShotTwo == 1)
            MoveReal(firstPose,qPickTwo,qRiseTwo,qMixer,qPour);
        end
        if (orderShotThree == 1)
            MoveReal(firstPose,qPickThree,qRiseThree,qMixer,qPour);
        end 
    end
    if (order == 2)
        if (orderShotOne == 2)
            MoveReal(firstPose,qPickOne,qRiseOne,qMixer,qPour);
        end
        if (orderShotTwo == 2)
            MoveReal(firstPose,qPickTwo,qRiseTwo,qMixer,qPour);
        end
        if (orderShotThree == 2)
            MoveReal(firstPose,qPickThree,qRiseThree,qMixer,qPour);
        end 
    end
    if (order == 3)
        if (orderShotOne == 3)
            MoveReal(firstPose,qPickOne,qRiseOne,qMixer,qPour);
        end
        if (orderShotTwo == 3)
            MoveReal(firstPose,qPickTwo,qRiseTwo,qMixer,qPour);
        end
        if (orderShotThree == 3)
            MoveReal(firstPose,qPickThree,qRiseThree,qMixer,qPour);
        end 
    end
    order = order + 1;
end
end

function MoveReal(firstPose,q1,q2,qMixer,qPour)
    MoveAllJoint(q2);
    MoveAllJoint(q1);
    Claw(14);
    pause(2);
    MoveAllJoint(q2);
    MoveAllJoint(qMixer);
    MoveAllJoint(qPour);
    MoveAllJoint(qMixer);
    MoveAllJoint(q2);
    MoveAllJoint(q1);
    Claw(26);
    pause(2);
    MoveAllJoint(q2);
    MoveAllJoint(qMixer)
end







