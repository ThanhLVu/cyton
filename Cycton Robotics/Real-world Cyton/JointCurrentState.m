% Create a subscriber to view the current state of each joint
function JointCurrentState()
%Setup variables
stateSub = rossubscriber('/joint_states');
receive(stateSub,2)
msg = stateSub.LatestMessage;
%To show joint angles and end effector position
msg.JointAngles
msg.Pos
end