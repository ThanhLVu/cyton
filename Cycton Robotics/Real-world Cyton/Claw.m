%Open and close claw
function Claw(dia)
%Setup variables
claw_client = rossvcclient('/ClawPosition');
claw_msg = rosmessage(claw_client);
%Publishing
claw_msg.Data = dia; % Values must be between 0 (closed) and 26.5 (open) ~ a millimeter value of the claw open amount.
claw_client.call(claw_msg);
end
