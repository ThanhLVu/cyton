%Move the robot home
function MoveHome()
%Setup variables
cute_home_client = rossvcclient('/goHome');
cute_home_client_msg = rosmessage(cute_home_client);
%Setting values and sending to the robot
cute_home_client.call(cute_home_client_msg);
end

