%Velocity control of joint angles
function JointVelocity()
%Setup Publisher and Message
cute_velocity_publisher = rospublisher('/cyton_velocity_commands');
cute_velocity_msg = rosmessage(cute_velocity_publisher);
%Publishing velocities
cute_velocity_msg.Data = [0.1,0,0,0,0,0,0];
cute_velocity_publisher.send(cute_velocity_msg);
end