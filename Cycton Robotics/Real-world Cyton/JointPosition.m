%Position control of joint angles
function JointPosition()
%Setup publisher and message
cute_position_publisher = rospublisher('/cyton_position_commands');
cute_position_msg = rosmessage(cute_position_publisher);
%Publishing position
cute_position_msg.Data = [0.5,0,0,0,0,0,0];
cute_position_publisher.send(cute_position_msg);
end
