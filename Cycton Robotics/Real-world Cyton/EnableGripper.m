function EnableGripper()
%Setup variables
cute_enable_gripper_client = rossvcclient('claw_controller/torque_enable')
cute_enable_gripper_msg = rosmessage(cute_enable_gripper_client);
%To Enable/Disable the gripper
cute_enable_gripper_msg.TorqueEnable = 1 %0
cute_enable_gripper_client.call(cute_enable_gripper_msg);
end

